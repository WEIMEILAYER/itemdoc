package com.spring.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

public class DateCalculate {

    /** 
     * 日期格式的运算 
     * @param args 
     */  
    public static void main(String[] args) {  
          
        Date now = new Date();  
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        System.out.println("------当前时间--------：" + sd.format(now));  
          
        //年: 加、减操作  
        System.out.println("1年之后："+sd.format(DateUtils.addYears(now, 1)));  
        System.out.println("1年之前："+sd.format(DateUtils.addYears(now, -1)));  
          
        //月: 加、减操作  
        System.out.println("1个月之后："+sd.format(DateUtils.addMonths(now, 1)));  
        System.out.println("1个月之前："+sd.format(DateUtils.addMonths(now, -1)));  
          
        //周: 加、减操作  
        System.out.println("1周之后："+sd.format(DateUtils.addWeeks(now, 1)));  
        System.out.println("1周之前："+sd.format(DateUtils.addWeeks(now, -1)));  
          
        //天: 加、减操作  
        System.out.println("昨天的这个时候：" + sd.format(DateUtils.addDays(now, -1)));  
        System.out.println("明天的这个时候：" + sd.format(DateUtils.addDays(now, 1)));  
          
        //小时: 加、减操作  
        System.out.println("1小时后：" + sd.format(DateUtils.addHours(now, 1)));  
        System.out.println("1小时前：" + sd.format(DateUtils.addHours(now, -1)));  
          
        //分钟: 加、减操作  
        System.out.println("1分钟之后："+sd.format(DateUtils.addMinutes(now, 1)));  
        System.out.println("1分钟之前："+sd.format(DateUtils.addMinutes(now, -1)));  
          
        //秒: 加、减操作  
        System.out.println("10秒之后："+sd.format(DateUtils.addSeconds(now, 10)));  
        System.out.println("10秒之前："+sd.format(DateUtils.addSeconds(now, -10)));  
          
        //毫秒: 加、减操作  
        System.out.println("1000毫秒之后："+sd.format(DateUtils.addMilliseconds(now, 1000)));  
        System.out.println("1000毫秒之前："+sd.format(DateUtils.addMilliseconds(now, -1000)));  
    }  
  
}  